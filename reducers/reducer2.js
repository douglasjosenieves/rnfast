export default (state = 'name', action) => {
	switch (action.type) {
		case 'SET_TEXT':
			return {text: action.payload.data};

		default:
			return state;
	}
};
