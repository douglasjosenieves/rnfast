/*
 * tipos de acciones
 */

export const set_text = data => {
	return {
		type: 'SET_TEXT',
		payload: {
			data,
		},
	};
};

/*export const SET_TEXT = 'SET_TEXT';
export const ADD_TODO = 'ADD_TODO';
export const COMPLETE_TODO = 'COMPLETE_TODO';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';*/

/*
 * otras constantes
 */

/*export const VisibilityFilters = {
	SHOW_ALL: 'SHOW_ALL',
	SHOW_COMPLETED: 'SHOW_COMPLETED',
	SHOW_ACTIVE: 'SHOW_ACTIVE',
};
*/
/*
 * creadores de acciones
 */

/*export function addTodo(data) {
	return {
		type: SET_TEXT,
		payload: {
			data,
		},
	};
}
*/
/*export function completeTodo(index) {
	return {type: COMPLETE_TODO, index};
}

export function setVisibilityFilter(filter) {
	return {type: SET_VISIBILITY_FILTER, filter};
}*/
