import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider, connect} from 'react-redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import {composeWithDevTools} from 'redux-devtools-extension';
import reducer from './reducers';

import RepoList from './RepoList';
import Router from './Router';

const client = axios.create({
	baseURL: 'https://api.github.com',
	responseType: 'json',
});

const store = createStore(
	reducer,
	composeWithDevTools(
		applyMiddleware(axiosMiddleware(client)),
		// other store enhancers if any
	),
);

export default class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<Router />
			</Provider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		marginTop: 50,
	},
});
